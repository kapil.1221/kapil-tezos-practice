import smartpy as sp

class TrainReservation(sp.Contract):
    def __init__(self):
        self.init(train_details = sp.map(tkey = sp.TString),
                  booking_details = sp.map(tkey = sp.TString, tvalue = sp.TList(t = sp.TString)), bookinglist = sp.list(t = sp.TString))
                  
    @sp.entryPoint
    def createJourney(self, params):
        self.checkJourney(params.tid)
        self.data.train_details[params.tid].name = params._name
        self.data.train_details[params.tid].source = params._source
        self.data.train_details[params.tid].destination = params._destination
        
    @sp.entryPoint
    def createBooking(self, params):
        sp.if (self.data.booking_details.contains(params.uid)):
            self.data.bookinglist = self.data.booking_details[params.uid]
            self.data.bookinglist.push(params._username)
            self.data.booking_details[params.uid] = self.data.bookinglist
             
        sp.else:
            self.data.bookinglist = []
            self.data.bookinglist.push(params._username)
            self.data.booking_details[params.uid] = self.data.bookinglist

    def checkJourney(self, tid):
        sp.if ~(self.data.train_details.contains(tid)):
            self.data.train_details[tid] = sp.record(name = "", source = "", destination = "")
            
@addTest(name = "Test")
def test():
    scenario = sp.testScenario()
    scenario.h1("Train Reservation")
    
    c1 = TrainReservation()
    scenario += c1

    scenario.h2("Create Train Journey")
    scenario += c1.createJourney(tid = "t1", _name = "Express", _source="Pune", _destination = "Bangalore")
    
    scenario.h2("Book Journey")
    scenario += c1.createBooking(uid = "u1", _username = "Kapil")