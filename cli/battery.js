#!/usr/bin/env node

'use strict';
const batteryLevel = require('battery-level');
const toPercent = require('to-percent');

batteryLevel().then(res => console.log(toPercent(res) + '%'));