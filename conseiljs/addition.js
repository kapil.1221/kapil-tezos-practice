const conseiljs = require('conseiljs');
const tezosNode = 'https://tezos-dev.cryptonomic-infra.tech';

conseiljs.setLogLevel('debug');

async function deployContract() {

      const keystore = {
          publicKey: 'edpktwFxH2vZgvcGSNFXvvnGxNM7ggJprE9BMnJXtmdtno9J5mttAB',
          privateKey: 'edskReS7AxaJXcxd3nDAeDjvjEG4vJJ5Xnzed44xMZDhYwKfpDRGBXUhszJ9DoQ2FXia2awA4AThE3wGjeN5xxpiYNGf2ymYXT',
          publicKeyHash: 'tz1TEFPH6Vw5KEXwEJhgBc5o238TFWUiKYqV',
          seed: '',
          storeType: conseiljs.StoreType.Fundraiser
      };


      const contractAddress = 'KT1B2cubRbMvFehGaGHCuyzPbg9hZghkxqVX';
      //const contractAddress = 'KT1QqF4pB8de46R2F9oasv8Qy3ttQtamEpAN';

      var augend=1221, addend=1;

      const params = `{"prim": "Pair", "args": [{"int": "${augend}"}, {"int": "${addend}"}]}`;
      // const params = `{"prim":"or","args":[
      //                                       {"prim":"or","args":[
      //                                                             {"prim": "Pair", "prim": "${addition}", "args": [{"int": "${augend}"}, {"int": "${addend}"},
      //                                                             {"prim": "Pair", "prim": "${multiplication}", "args": [{"int": "${augend}"}, {"int": "${addend}"},
      //                                                             {"prim": "Pair", "prim": "${division}", "args": [{"int": "${augend}"}, {"int": "${addend}"}
      //                                                             ]}`;

      const result = await conseiljs.TezosNodeWriter.sendContractInvocationOperation(tezosNode, keystore, contractAddress,  0, 50000, '', 1000, 20000, undefined, 
                                                                                      params, conseiljs.TezosParameterFormat.Micheline);

      console.log(`Injected operation group id ${result.operationGroupID}`);

    }

deployContract();