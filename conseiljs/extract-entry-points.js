const conseiljs = require('conseiljs');
const tezosNode = 'https://tezos-dev.cryptonomic-infra.tech';

conseiljs.setLogLevel('debug');

async function deployContract() {

      const contractAddress = 'KT1QqF4pB8de46R2F9oasv8Qy3ttQtamEpAN';
      
      const contractParameters = `parameter (or (or (pair %addition (nat %a) (nat %b)) (pair %division (nat %a) (nat %b))) (pair %multiplication (nat %a) (nat %b)));`;

      const entryPoints = await conseiljs.TezosContractIntrospector.generateEntryPointsFromParams(contractParameters);

      entryPoints.forEach(p => {
              console.log(`${p.name}(${p.parameters.map(pp => (pp.name || 'unnamed') + '/' + pp.type).join(', ')})`);
          });

          console.log(entryPoints[0].generateParameter('', ''));
          console.log(entryPoints[1].generateParameter('', ''));
          console.log(entryPoints[2].generateParameter('', ''));
      }


deployContract();
