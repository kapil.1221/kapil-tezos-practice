const conseiljs = require('conseiljs');
const tezosNode = 'https://rpcalpha.tzbeta.net';

conseiljs.setLogLevel('debug');

async function interrogateContract() {
const contractParameters = `parameter (or (or (unit %buyApple) (unit %buyGuava)) (unit %buyMango));`;

 const entryPoints = await conseiljs.TezosContractIntrospector.generateEntryPointsFromParams(contractParameters);
entryPoints.forEach(p => {
console.log(`${p.name}(${p.parameters.map(pp => (pp.name || 'unnamed') + '/' + pp.type).join(', ')})`);
}); 

console.log(entryPoints[0].generateParameter(''));
console.log(entryPoints[1].generateParameter(''));
console.log(entryPoints[2].generateParameter(''));
}

interrogateContract();